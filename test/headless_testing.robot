*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}  https://demoblaze.com/
${Browser}      chrome

*** Test Cases ***
Opening and Closing Browser
    open browser    ${URL}  headlesschrome
    click element    id:signin2
    wait until element is visible   xpath://*[@id="signInModalLabel"]
    input text    id:sign-username    halloasdf
    input password    id:sign-password   hallo12345t345
    sleep    3
    click button    Sign up
    alert should be present    This user already exist.
    close browser